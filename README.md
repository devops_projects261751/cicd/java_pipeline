# Java Pipeline 

### Project contain sample java code that will be used to create CI pipeline.

> Flow -> git -> .gitlab-ci.yml -> mvn (compile + test + pacakge) -> docker image 

Manual testing - 
```
# apt install -y openjdk-11-jdk
# apt install maven 

# mvn clean package -Dproject.version=1.0.0 -Dproject.artifact=sbdevopsx-java-app
```

#### Important Link 

[Java+Maven+Gitlab](https://cmakkaya.medium.com/gitlab-ci-cd-1-building-a-java-project-using-maven-and-docker-within-the-gitlab-ci-pipeline-278feaf7ee12)
