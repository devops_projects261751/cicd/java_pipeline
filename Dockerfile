FROM openjdk:8-alpine
LABEL maintainer="sbdevopsx"

WORKDIR /app
COPY artifacts/*.jar /app/app.jar

CMD ["java","-jar","app.jar"]